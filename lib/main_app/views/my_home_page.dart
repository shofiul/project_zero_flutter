import 'package:flutter/material.dart';
import 'package:projectzeroflutter/features/counter/view_model/counter_view_model.dart';
import 'package:provider/provider.dart';

import '../strings.dart';

class MyHomePage extends StatefulWidget {

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(Strings.homeScreenTitle),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
                "${Strings.buttonPushedMessage}",
            ),
            Consumer<CounterViewModel>(builder: (context, counterViewModel, c) {
              return Text(
                '${counterViewModel.counterValue}',
                style: Theme.of(context).textTheme.display1,
              );
            }),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: Provider.of<CounterViewModel>(context, listen: false)
            .incrementCounter,
        tooltip: Strings.incrementText,
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
