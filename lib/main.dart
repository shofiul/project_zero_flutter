import 'package:flutter/material.dart';
import 'package:projectzeroflutter/features/counter/view_model/counter_view_model.dart';
import 'package:projectzeroflutter/main_app/views/my_home_page.dart';
import 'package:provider/provider.dart';

import 'main_app/strings.dart';


/// This is demo project to show basic structure of flutter app

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    final providers = [
      ChangeNotifierProvider(
        create: (BuildContext context) => CounterViewModel(),
      ),
    ];

    return MultiProvider(
      providers: providers,
      child: MaterialApp(
        title: Strings.appName,
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: MyHomePage(),
      ),
    );
  }
}
