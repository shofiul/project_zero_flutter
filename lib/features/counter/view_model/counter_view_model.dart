import 'package:flutter/cupertino.dart';

class CounterViewModel with ChangeNotifier{

  int _counter = 0;

  void incrementCounter() {
    _counter++;
    notifyListeners();
  }

  int get counterValue => _counter;

//  set counter(int value) {
//    _counter = value;
//  }


}