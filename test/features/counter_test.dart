

import 'package:flutter_test/flutter_test.dart';
import 'package:projectzeroflutter/features/counter/view_model/counter_view_model.dart';

main(){

  var counterViewModel = CounterViewModel();


  test("Counter Test,Initial value should be 0 ",(){
    var value = counterViewModel.counterValue;
    expect(value, 0);
  });

  test('Counter Test, Increment method call one time, counter value should be 1',(){
    counterViewModel.incrementCounter();
    var value = counterViewModel.counterValue;
    expect(value, 1);

  });

}